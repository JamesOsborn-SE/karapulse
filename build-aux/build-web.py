#!/usr/bin/env python3

import sys
import subprocess
import os
import shutil
import glob
import platform

env = os.environ

(MESON_BUILD_ROOT, MESON_SOURCE_ROOT, NPM_CACHE) = sys.argv[1:]

NPM_PATH = os.path.join(MESON_SOURCE_ROOT, "web")
NPM_EXE = "npm"

if platform.system() == "Windows":
    NPM_EXE += ".cmd"

if os.path.isdir(NPM_CACHE):
    CMD = [NPM_EXE, "install", "--offline", "--cache=" + NPM_CACHE]
else:
    CMD = [NPM_EXE, "install"]

subprocess.run(CMD, env=env, cwd=NPM_PATH)

NG_EXE = os.path.join(NPM_PATH, "node_modules", ".bin", "ng")
if platform.system() == "Windows":
    NG_EXE += ".cmd"

CMD = [NG_EXE, "build", "--output-hashing=none"]

subprocess.run(CMD, env=env, cwd=NPM_PATH)
web_files = glob.glob(os.path.join(NPM_PATH, "dist", "karaoke-ng", "*"))
for f in web_files:
    print(f)
    shutil.copy2(f, os.path.join(MESON_BUILD_ROOT, "web"))
