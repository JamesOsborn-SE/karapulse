// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use std::{
    path::{Path, PathBuf},
    sync::Once,
};

use anyhow::Result;
use gstreamer as gst;

static GTK_INIT: Once = Once::new();
static CDG_REGISTER: Once = Once::new();
static TEXTAHEAD_REGISTER: Once = Once::new();
static SPOTIFY_REGISTER: Once = Once::new();
static GST_GTK4_REGISTER: Once = Once::new();

pub fn init() -> Result<()> {
    std::env::set_var("GST_XINITTHREADS", "1");
    gst::init()?;

    GTK_INIT.call_once(|| {
        gtk::init().expect("failed to init GTK");
    });

    #[cfg(feature = "cdg-plugin")]
    {
        CDG_REGISTER
            .call_once(|| gstcdg::plugin_register_static().expect("failed to register cdg plugin"));
    }

    TEXTAHEAD_REGISTER.call_once(|| {
        gsttextahead::plugin_register_static().expect("failed to register textahead plugin")
    });

    SPOTIFY_REGISTER.call_once(|| {
        gstspotify::plugin_register_static().expect("failed to register spotify plugin")
    });

    GST_GTK4_REGISTER.call_once(|| {
        gstgtk4::plugin_register_static().expect("failed to register gstgtk4 plugin")
    });

    // Disable vaapi as it breaks rendering with gtkglsink
    let registry = gst::Registry::get();
    let plugin = registry.lookup("libgstvaapi.so");
    if let Some(plugin) = plugin {
        registry.remove_plugin(&plugin);
    }

    Ok(())
}

fn karapulse_dir(mut path: PathBuf, name: &str) -> anyhow::Result<PathBuf> {
    path.push("karapulse");
    std::fs::create_dir_all(&path)?;
    path.push(name);

    Ok(path)
}

pub fn user_data_dir_file(name: &str) -> anyhow::Result<PathBuf> {
    karapulse_dir(gtk::glib::user_data_dir(), name)
}

pub fn config_dir_file(name: &str) -> anyhow::Result<PathBuf> {
    karapulse_dir(gtk::glib::user_config_dir(), name)
}

pub fn cache_dir_file(name: &str) -> anyhow::Result<PathBuf> {
    karapulse_dir(gtk::glib::user_cache_dir(), name)
}

#[cfg(not(target_os = "windows"))]
pub fn install_data_dir_file(name: &str) -> anyhow::Result<PathBuf> {
    let path = Path::new(crate::config::PKGDATADIR).join(name);
    Ok(path)
}

#[cfg(target_os = "windows")]
pub fn install_data_dir_file(name: &str) -> anyhow::Result<PathBuf> {
    // paths are defined during installation on Windows, not at build, and so are relative to the current path
    let path = Path::new("share").join("karapulse").join(name);
    Ok(path)
}

#[cfg(not(target_os = "windows"))]
pub fn gettext(s: &str) -> String {
    gettextrs::gettext(s)
}

#[cfg(target_os = "windows")]
pub fn gettext(s: &str) -> String {
    s.to_string()
}
