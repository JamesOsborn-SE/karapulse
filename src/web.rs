// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
use gstreamer::glib::Sender;
use rocket::figment::Figment;
use rocket::fs::FileServer;
use std::net::{IpAddr, Ipv4Addr};
use std::path::Path;
use std::sync::Mutex;
use std::time::{Duration, Instant};
use tokio::sync::oneshot;
use tokio::time::timeout;

use anyhow::anyhow;
use rocket::config::Config;
use rocket::http::Method;
use rocket::serde::json::Json;
use rocket::{get, post, routes, Build, Rocket, State};
use rocket_cors::{AllowedHeaders, AllowedOrigins};

use crate::karapulse::{Message, Reply, Song};
use crate::{common, protocol, settings, spotify};
use crate::{db::DB, settings::Settings};

type Result<T, E = rocket::response::Debug<anyhow::Error>> = std::result::Result<T, E>;

// The minimum of time between two valid 'next' request.
// This is used to prevent next collisions when different users want to next the same song.
pub const NEXT_IGNORE_WINDOW: Duration = Duration::from_secs(5);

/// Query Spotify if DB search did not returned at least that many results.
pub const SPOTIFY_RESULT_THRESHOLD: usize = 40;

/// Minimum length of search fields to search Spotify; prevent spamming Spotify while users is typing
pub const SPOTITY_SEARCH_MIN_LEN: usize = 5;

struct Web {
    settings: Settings,
    tx: Mutex<Sender<Message>>,
    db: Mutex<DB>,
    spotify: Option<spotify::Spotify>,
    last_next: Mutex<Option<Instant>>,
}

impl Web {
    fn new(
        settings: Settings,
        tx: Sender<Message>,
        db: DB,
        spotify: Option<spotify::Spotify>,
    ) -> Web {
        Web {
            settings,
            tx: Mutex::new(tx),
            db: Mutex::new(db),
            spotify,
            last_next: Mutex::new(None),
        }
    }

    fn jwt_encode(&self, claims: &Claims) -> String {
        let header = jsonwebtoken::Header::new(jsonwebtoken::Algorithm::HS512);
        let secret = self.settings.admin_jwt_secret();
        let encoding_key = jsonwebtoken::EncodingKey::from_secret(secret.as_ref());

        jsonwebtoken::encode(&header, &claims, &encoding_key).unwrap()
    }

    fn jwt_decode(&self, token: &str) -> Result<Claims, jsonwebtoken::errors::Error> {
        let token = token.trim_start_matches("Bearer").trim();
        let secret = self.settings.admin_jwt_secret();
        let decoding_key = jsonwebtoken::DecodingKey::from_secret(secret.as_ref());
        let validation = jsonwebtoken::Validation::new(jsonwebtoken::Algorithm::HS512);

        jsonwebtoken::decode::<Claims>(token, &decoding_key, &validation).map(|token| token.claims)
    }
}

#[get("/play")]
fn play(web: &State<Web>) {
    let tx = web.tx.lock().unwrap();
    tx.send(Message::Play).unwrap();
}

#[get("/pause")]
fn pause(web: &State<Web>) {
    let tx = web.tx.lock().unwrap();
    tx.send(Message::Pause).unwrap();
}

#[get("/next")]
fn next(web: &State<Web>) {
    {
        let mut last = web.last_next.lock().unwrap();
        if let Some(last) = *last {
            if last.elapsed() < NEXT_IGNORE_WINDOW {
                debug!("Ignore consecutive 'next' requests");
                return;
            }
        }
        *last = Some(Instant::now());
    }
    let tx = web.tx.lock().unwrap();
    tx.send(Message::Next).unwrap();
}

#[get("/search/<fields>")]
async fn search(web: &State<Web>, fields: String) -> Result<Json<protocol::SearchResponse>> {
    debug!("search: {}", fields);
    let mut songs: Vec<protocol::Song> = {
        let db = web.db.lock().unwrap();
        let songs = db.search(&fields)?;
        songs.into_iter().map(protocol::Song::from_db).collect()
    };

    if songs.len() >= SPOTIFY_RESULT_THRESHOLD {
        debug!(
            "{fields}: found {} songs in DB, don't query Spotify",
            songs.len()
        );
    } else if fields.len() >= SPOTITY_SEARCH_MIN_LEN {
        if let Some(spotify) = web.spotify.as_ref() {
            debug!("{fields}: found {} songs in DB, query Spotify", songs.len());
            match spotify.search(&fields).await {
                Ok(spotify_songs) => {
                    debug!("{fields}: found {} songs from Spotify", spotify_songs.len());

                    for s in spotify_songs.into_iter() {
                        songs.push(protocol::Song::from_spotify(s));
                    }
                }
                Err(err) => {
                    warn!("Failed to search on Spotify: {}", err);
                }
            }
        }
    }

    let mut result = protocol::SearchResponse::new(songs);

    if let Some(spotify) = web.spotify.as_ref() {
        result.retrieve_cover(spotify).await;
    }

    Ok(Json(result))
}

// FIXME: this is kind of hacky. We should break the protocol to:
// - rename this API 'enqueue'
// - pass the type or song instead of parsing the ID as a string
#[get("/enqueue_db/<id>/<user>")]
async fn enqueue_db(web: &State<Web>, id: protocol::SongId, user: String) -> Option<()> {
    let song = match id {
        protocol::SongId::Db(id) => {
            debug!("query DB for song id {}", id);
            let db = web.db.lock().unwrap();
            match db.find_song(id) {
                Ok(song) => Song::Db(song),
                Err(err) => {
                    warn!("did not find song {}: {:?}", id, err);
                    return None;
                }
            }
        }
        protocol::SongId::Spotify(id) => {
            if let Some(spotify) = web.spotify.as_ref() {
                match spotify.song_from_id(&id).await {
                    Ok(song) => Song::Spotify(song),
                    Err(err) => {
                        warn!("failed to retrieve Spotify track {}: {:?}", id, err);
                        return None;
                    }
                }
            } else {
                error!("spotify not configured");
                return None;
            }
        }
    };

    let info = song.info().unwrap();
    debug!("enqueue: '{} - {}' from {}", info.artist, info.title, user);

    let tx = web.tx.lock().unwrap();
    tx.send(Message::Enqueue { user, song }).unwrap();
    Some(())
}

#[get("/remove_queue/<song_id>")]
fn remove_queue(web: &State<Web>, song_id: protocol::SongId) {
    let tx = web.tx.lock().unwrap();
    tx.send(Message::RemoveSong { song_id }).unwrap();
}

#[get("/status")]
async fn status(web: &State<Web>) -> Result<Json<protocol::StatusResponse>> {
    let rx = {
        let tx = web.tx.lock().unwrap();
        let (reply_tx, rx) = oneshot::channel();

        tx.send(Message::GetStatus {
            reply_tx: Some(reply_tx),
        })
        .unwrap();
        rx
    };

    let d = Duration::from_secs(1);
    match timeout(d, rx).await {
        Ok(Ok(Reply::Status { mut status })) => {
            if let Some(spotify) = web.spotify.as_ref() {
                // FIXME: hacky, should be done when retrieving the status but we can't run async code from
                // the Receiver attach callback.
                status.retrieve_cover(spotify).await;
            }

            Ok(Json(status))
        }
        _ => {
            let msg = "status reply timeout";
            warn!("{}", msg);
            Err(rocket::response::Debug(anyhow!(msg)))
        }
    }
}

#[get("/history")]
async fn history(web: &State<Web>) -> Result<Json<protocol::HistoryResponse>> {
    let history = {
        let db = web.db.lock().unwrap();
        db.list_history()?
    };

    let mut result = protocol::HistoryResponse::new(history, web.spotify.as_ref()).await;

    if let Some(spotify) = web.spotify.as_ref() {
        result.retrieve_cover(spotify).await;
    }

    Ok(Json(result))
}

#[get("/history/<user>")]
async fn history_for(web: &State<Web>, user: String) -> Result<Json<protocol::HistoryResponse>> {
    let history = {
        let db = web.db.lock().unwrap();
        db.history_for(&user)?
    };

    let result = protocol::HistoryResponse::new(history, web.spotify.as_ref()).await;
    Ok(Json(result))
}

#[get("/all")]
fn all(web: &State<Web>) -> Result<Json<Vec<protocol::Song>>> {
    debug!("all");
    let songs = {
        let db = web.db.lock().unwrap();
        let songs = db.list_songs()?;
        songs.into_iter().map(protocol::Song::from_db).collect()
    };

    Ok(Json(songs))
}

#[get("/restart-song")]
fn restart_song(web: &State<Web>) {
    debug!("restart song");
    let tx = web.tx.lock().unwrap();
    tx.send(Message::RestartSong).unwrap();
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
struct Claims {
    is_admin: bool,

    exp: usize,
}

impl Claims {
    fn new_admin() -> Self {
        let exp = chrono::Utc::now()
            .checked_add_signed(chrono::Duration::days(1))
            .expect("Invalid timestamp")
            .timestamp();

        Self {
            is_admin: true,
            exp: exp as usize,
        }
    }
}

#[derive(Debug)]
struct Jwt {
    claims: Claims,
}

impl Jwt {
    fn is_admin(&self) -> Result<(), NetworkError> {
        if self.claims.is_admin {
            Ok(())
        } else {
            Err(NetworkError::Unauthorized("not admin".to_string()))
        }
    }
}

use rocket::http::Status;
use rocket::request::{FromRequest, Outcome, Request};
use rocket::Responder;

#[derive(Responder, Debug)]
pub enum NetworkError {
    #[response(status = 401)]
    Unauthorized(String),
}

// inspired from https://brookjeynes.dev/posts/jwt-authentication-rust
#[rocket::async_trait]
impl<'r> FromRequest<'r> for Jwt {
    type Error = NetworkError;

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, NetworkError> {
        let web = req.guard::<&State<Web>>().await.unwrap();

        match req.headers().get_one("authorization") {
            None => Outcome::Error((
                Status::Unauthorized,
                NetworkError::Unauthorized(
                    "Error validating JWT token - No token provided".to_string(),
                ),
            )),
            Some(key) => match web.jwt_decode(key) {
                Ok(claims) => Outcome::Success(Jwt { claims }),
                Err(err) => match &err.kind() {
                    jsonwebtoken::errors::ErrorKind::ExpiredSignature => Outcome::Error((
                        Status::Unauthorized,
                        NetworkError::Unauthorized(
                            "Error validating JWT token - Expired Token".to_string(),
                        ),
                    )),
                    jsonwebtoken::errors::ErrorKind::InvalidToken => Outcome::Error((
                        Status::Unauthorized,
                        NetworkError::Unauthorized(
                            "Error validating JWT token - Invalid Token".to_string(),
                        ),
                    )),
                    _ => Outcome::Error((
                        Status::Unauthorized,
                        NetworkError::Unauthorized(format!("Error validating JWT token - {}", err)),
                    )),
                },
            },
        }
    }
}

/* admin */

#[post("/admin/auth", data = "<req>")]
fn admin_auth(
    web: &State<Web>,
    req: Json<protocol::AdminAuthRequest>,
) -> Result<Json<protocol::AdminAuthReply>, NetworkError> {
    if web.settings.admin_check_password(&req.password) {
        debug!("admin auth success");
        // generate JWT token
        let claims = Claims::new_admin();
        let token = web.jwt_encode(&claims);

        Ok(Json(protocol::AdminAuthReply { token }))
    } else {
        warn!("admin auth failed");
        Err(NetworkError::Unauthorized(
            "wrong admin password".to_string(),
        ))
    }
}

#[get("/admin/config")]
fn admin_config_get(
    web: &State<Web>,
    auth: Result<Jwt, NetworkError>,
) -> Result<Json<protocol::ConfigReply>, NetworkError> {
    // can be called without token if password is not defined yet
    if !web.settings.admin_check_password("") {
        let auth = auth?;
        auth.is_admin()?;
    }

    let config = web.settings.export();
    Ok(Json(protocol::ConfigReply {
        config,
        token: None,
    }))
}

#[post("/admin/config", data = "<settings>")]
fn admin_config_post(
    web: &State<Web>,
    auth: Result<Jwt, NetworkError>,
    settings: Json<settings::Inner>,
) -> Result<Json<protocol::ConfigReply>, NetworkError> {
    let new_settings = settings.into_inner();

    let jwt_updated = if web.settings.admin_check_password("") {
        // no password set yet, only allow to change password
        web.settings.admin_set_password(new_settings.admin.password);
        debug!("password is now set");
        true
    } else {
        let auth = auth?;
        auth.is_admin()?;

        debug!("updating settings");
        web.settings.update(new_settings)
    };

    let token = if jwt_updated {
        // generate new JWT token
        let claims = Claims::new_admin();
        let token = web.jwt_encode(&claims);
        Some(token)
    } else {
        None
    };

    let config = web.settings.export();
    Ok(Json(protocol::ConfigReply { config, token }))
}

fn rocket(
    path: &Path,
    settings: Settings,
    tx: Sender<Message>,
    db: DB,
    spotify: Option<spotify::Spotify>,
) -> Rocket<Build> {
    let web = Web::new(settings, tx, db, spotify);

    let allowed_origins = AllowedOrigins::all();

    let cors = rocket_cors::CorsOptions {
        allowed_origins,
        allowed_methods: vec![Method::Get].into_iter().map(From::from).collect(),
        allowed_headers: AllowedHeaders::some(&["Authorization", "Accept"]),
        allow_credentials: true,
        ..Default::default()
    }
    .to_cors()
    .unwrap();

    let mut config = Config::release_default();
    config.port = 1848;
    config.address = IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0));
    config.shutdown.ctrlc = false;
    let figment = Figment::from(config);

    rocket::custom(figment)
        .mount(
            "/api",
            routes![
                play,
                pause,
                next,
                search,
                enqueue_db,
                status,
                history,
                history_for,
                all,
                restart_song,
                remove_queue,
                admin_auth,
                admin_config_get,
                admin_config_post,
            ],
        )
        .mount("/", FileServer::from(path))
        .manage(web)
        .attach(cors)
}

pub async fn start_web(
    settings: Settings,
    tx: Sender<Message>,
    db: DB,
    spotify: Option<spotify::Spotify>,
) -> anyhow::Result<()> {
    let mut path = common::install_data_dir_file("web")?;
    if !path.exists() {
        // look into sources
        path = Path::new("web").join("dist").join("karaoke-ng");
    }

    let path = path
        .canonicalize()
        .map_err(|e| anyhow!("Failed finding web directory ({}): {}", path.display(), e))?;

    debug!("serving web app: {}", path.display());

    let _rocket = rocket(&path, settings, tx, db, spotify).launch().await?;

    Ok(())
}

impl<'r> rocket::request::FromParam<'r> for protocol::SongId {
    type Error = &'r str;

    fn from_param(param: &'r str) -> Result<Self, Self::Error> {
        param.parse().map_err(|_| param)
    }
}

#[cfg(test)]
mod test {
    use super::rocket;
    use crate::karapulse;
    use crate::karapulse::{Message, Reply, Song};
    use crate::queue::Queue;
    use crate::{db, protocol};
    use crate::{db::DB, settings};

    use gstreamer::glib::MainContext;
    use rocket::http::Status;
    use rocket::local::asynchronous::Client;
    use serde_json::{json, Value};
    use std::cell::RefCell;
    use std::env;
    use std::path::PathBuf;
    use std::rc::Rc;
    use std::sync::Arc;
    use std::time::Duration;

    struct Test {
        client: Arc<Client>,
        // Need to keep the context alive during the test duration
        context: MainContext,
        message: Rc<RefCell<Option<Message>>>,
    }

    impl Test {
        fn new(
            client: Arc<Client>,
            context: MainContext,
            message: Rc<RefCell<Option<Message>>>,
        ) -> Test {
            Self {
                client,
                context,
                message,
            }
        }

        async fn received_message(&self) -> Option<Message> {
            while self.message.borrow().is_none() {
                tokio::time::sleep(Duration::from_millis(100)).await;
                self.context.iteration(false);
            }
            self.message.replace(None)
        }

        async fn get_query(&self, query: &'static str) -> (Status, Option<String>) {
            let response = self.client.get(query).dispatch().await;
            (response.status(), response.into_string().await)
        }

        async fn get_no_reply(&self, query: &'static str) {
            let (status, response) = self.get_query(query).await;
            assert_eq!(status, Status::Ok);
            assert_eq!(response, None);
        }

        async fn get_json(&self, query: &'static str) -> Value {
            let (status, response) = self.get_query(query).await;
            assert_eq!(status, Status::Ok);
            serde_json::from_str(&response.unwrap()).unwrap()
        }
    }

    fn populate_db(db: &DB) {
        db.add_song(&PathBuf::from("file1.mp3"), "artist1", "title1", Some(10))
            .expect("Failed adding song");

        let songs = db.list_songs().expect("Failed listing songs");

        let song = &songs[0];
        let id = db
            .add_history_db(song, "Alice")
            .expect("Failed adding history");
        db.set_history_played(id).unwrap();
    }

    async fn setup() -> Test {
        #[allow(deprecated)]
        let (tx, rx) = gstreamer::glib::MainContext::channel(gstreamer::glib::Priority::DEFAULT);
        let db = DB::new_memory().expect("Failed creating DB");

        populate_db(&db);

        let context = gstreamer::glib::MainContext::new();
        let guard = context.acquire().unwrap();

        let message = Rc::new(RefCell::new(None));
        let message_clone = message.clone();
        rx.attach(Some(&context), move |mut msg| {
            let message = &message_clone;

            if let Message::GetStatus { ref mut reply_tx } = msg {
                let reply_tx = reply_tx.take().unwrap();
                let mut queue = Queue::new();

                let song1 = db::Song {
                    rowid: 1,
                    path: "file1.mp3".to_string(),
                    artist: "artist1".to_string(),
                    title: "title1".to_string(),
                    length: Some(10),
                };
                queue.add("alice", Song::Db(song1), None).unwrap();

                let song2 = db::Song {
                    rowid: 2,
                    path: "file2.mp3".to_string(),
                    artist: "artist2".to_string(),
                    title: "title2".to_string(),
                    length: Some(15),
                };
                queue.add("bob", Song::Db(song2), None).unwrap();

                let current_song = queue
                    .next_item()
                    .map(|s| protocol::StatusResponseSong::new(s, None).unwrap());
                let queue = queue
                    .snapshot()
                    .into_iter()
                    .map(|s| protocol::StatusResponseSong::new(s, Some(5)).unwrap())
                    .collect();

                let status = protocol::StatusResponse::new(
                    karapulse::State::Playing,
                    current_song,
                    Some(5),
                    queue,
                );
                reply_tx
                    .send(Reply::Status { status })
                    .expect("Failed sending status reply");
            };

            message.replace(Some(msg));
            gstreamer::glib::ControlFlow::Continue
        });

        // Can't serve the actual web client as it may not be build
        let path = env::temp_dir();

        let settings_file = tempfile::NamedTempFile::new().unwrap();
        let settings = settings::Settings::new_test(settings_file.path());

        let client = Arc::new(
            Client::tracked(rocket(&path, settings, tx, db, None))
                .await
                .expect("valid rocket instance"),
        );

        drop(guard);
        Test::new(client, context, message)
    }

    #[rocket::async_test]
    async fn play() {
        let test = setup().await;
        test.get_no_reply("/api/play").await;
        assert_eq!(test.received_message().await, Some(Message::Play));
    }

    #[rocket::async_test]
    async fn pause() {
        let test = setup().await;
        test.get_no_reply("/api/pause").await;
        assert_eq!(test.received_message().await, Some(Message::Pause));
    }

    #[rocket::async_test]
    async fn next() {
        let test = setup().await;
        test.get_no_reply("/api/next").await;
        assert_eq!(test.received_message().await, Some(Message::Next));
    }

    #[rocket::async_test]
    async fn search() {
        let test = setup().await;
        assert_eq!(test.get_json("/api/search/nope").await, json!([]));
        assert_eq!(
            test.get_json("/api/search/artist1").await,
            json!([ { "id": "db-1", "artist": "artist1", "title": "title1", "length": 10, "type": "cdg", "cover":  serde_json::Value::Null, "explicit": false } ]),
        );
    }

    #[rocket::async_test]
    async fn enqueue_db() {
        let test = setup().await;

        test.get_no_reply("/api/enqueue_db/db-1/test").await;
        assert_eq!(
            test.received_message().await,
            Some(Message::Enqueue {
                user: "test".to_string(),
                song: Song::Db(db::Song {
                    rowid: 1,
                    path: "file1.mp3".to_string(),
                    artist: "artist1".to_string(),
                    title: "title1".to_string(),
                    length: Some(10),
                })
            })
        );

        let (status, _response) = test.get_query("/api/enqueue_db/100/test").await;
        assert_eq!(status.code, 404);
    }

    #[rocket::async_test]
    async fn status() {
        let test = setup().await;
        let response = test.client.get("/api/status").dispatch();
        let message = test.received_message();
        let (response, _) = tokio::join!(response, message);
        let (status, response) = (response.status(), response.into_string().await);
        assert_eq!(status, Status::Ok);
        let reply: Value = serde_json::from_str(&response.unwrap()).unwrap();
        assert_eq!(
            reply,
            json!({ "current_song": {"song": { "artist": "artist1", "id": "db-1", "length": 10, "title": "title1", "type": "cdg", "cover":  serde_json::Value::Null, "explicit": false },
                "user": "alice"},
                "position": 5,
                "queue": [ {"song": { "artist": "artist2", "id": "db-2", "length": 15, "title": "title2", "type": "cdg", "cover":  serde_json::Value::Null, "explicit": false }, "user": "bob", "eta": 5 } ],
                "state": "Playing" })
        );
    }

    #[rocket::async_test]
    async fn history() {
        let test = setup().await;

        fn check_history(result: Value) {
            let result = result.as_array().unwrap();
            assert_eq!(result.len(), 1);
            let result = &result[0];
            assert_eq!(result["id"], 1);
            assert!(!result["played"].is_null());
            assert!(!result["queued"].as_str().unwrap().is_empty());
            assert_eq!(result["user"], "Alice");
            assert_eq!(
                result["song"],
                json!( { "id": "db-1", "artist": "artist1", "title": "title1", "length": 10, "type": "cdg", "cover":  serde_json::Value::Null, "explicit": false })
            );
        }

        check_history(test.get_json("/api/history").await);
        assert_eq!(test.get_json("/api/history/nope").await, json!([]));
        check_history(test.get_json("/api/history/Alice").await);
    }

    #[rocket::async_test]
    async fn all() {
        let test = setup().await;
        assert_eq!(
            test.get_json("/api/all").await,
            json!([ { "id": "db-1", "artist": "artist1", "title": "title1", "length": 10, "type": "cdg", "cover":  serde_json::Value::Null, "explicit": false } ]),
        );
    }

    #[rocket::async_test]
    async fn restart_song() {
        let test = setup().await;
        test.get_no_reply("/api/restart-song").await;
        assert_eq!(test.received_message().await, Some(Message::RestartSong));
    }
}
