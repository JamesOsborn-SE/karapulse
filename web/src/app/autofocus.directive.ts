import { Directive, ElementRef } from '@angular/core';

// From
// https://netbasal.com/autofocus-that-works-anytime-in-angular-apps-68cb89a3f057

@Directive({
  selector: '[autofocus]'
})
export class AutofocusDirective {
  constructor(private host: ElementRef) {}

  ngAfterViewInit() {
    this.host.nativeElement.focus();
  }
}
