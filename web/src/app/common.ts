export interface AuthResponse {
  token: string
}


export type ConfigData = {
  admin: {
    password: string
  }
}

export type ConfigBody = {
  config: ConfigData
  token?: string
}
