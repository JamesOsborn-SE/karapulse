import {Component, Inject} from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef, MatDialogModule} from '@angular/material/dialog';
import {KaraokeService} from "../karaoke.service";
import {catchError} from "rxjs/operators";
import {HttpErrorResponse} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import { AuthResponse } from '../common';


export interface DialogData {
  adminPassword: string;
}

// dialog
@Component({
  selector: 'app-config-page-login',
  templateUrl: './config-page-login.component.html',
  styleUrls: ['./config-page-login.component.css'],
})
export class ConfigPageLoginComponent {
  public state: string = ''
  public errorMessage: undefined | string = undefined

  constructor(
    public dialogRef: MatDialogRef<ConfigPageLoginComponent>,
    private karaokeService: KaraokeService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  private handleError = (error: HttpErrorResponse, caught: Observable<AuthResponse>) => {
    console.log(error);
    if (error.status === 401) {
      this.state = 'error'
      this.errorMessage = 'Invalid password'
    } else {
      this.state = 'error';
      this.errorMessage = 'Something bad happened; please try again later.'
    }

    return throwError(() => new Error(this.errorMessage));
  };

  onLoginClick(): void {
    console.log(this.data.adminPassword)
    this.state = 'loading'
    this.errorMessage = ''
    this.karaokeService.authAdmin(this.data.adminPassword ?? '')
      .pipe(
        catchError(this.handleError)
      ).subscribe((data)=> {
        this.dialogRef.close(data.token);
    });
  }
}
