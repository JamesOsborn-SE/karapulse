import {Component, OnInit} from '@angular/core';
import {ConfigPageLoginComponent} from "../config-page-login/config-page-login.component";
import {MatDialog} from "@angular/material/dialog";
import {UserService} from "../user.service";
import {KaraokeService} from "../karaoke.service";
import {catchError} from "rxjs/operators";
import {HttpErrorResponse} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {Router} from "@angular/router";
import {FormBuilder} from "@angular/forms";
import {ConfigBody, ConfigData} from "../common";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Overlay} from "@angular/cdk/overlay";

@Component({
  selector: 'app-config-page',
  templateUrl: './config-page.component.html',
  styleUrls: ['./config-page.component.css'],
  providers: [UserService, MatSnackBar, MatDialog, FormBuilder]
})
export class ConfigPageComponent implements OnInit {
  configJson: any
  config = this.fb.group({
    password: ''
  })

  // this is getting a bit too big, we should split it in multiple component
  constructor(public dialog: MatDialog, public userService: UserService, private karaokeService: KaraokeService,
              private router: Router, private fb: FormBuilder, private _snackBar: MatSnackBar) {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfigPageLoginComponent, {
      data: {},
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('Auth dialog was closed with', result);
      this.userService.profile.token = result;
      this.userService.save();
      if (this.userService.profile.token) {
        this.getConfig();
      }
    });
  }


  ngOnInit(): void {
    if (this.userService.profile.token) {
      this.getConfig();
    }

    if (!this.userService.profile.token)
      this.openDialog();
  }

  private getConfig = () => {
    if (!this.userService.profile.token) {
      throw new Error('should not happen')
    }
    this.karaokeService.getConfig(this.userService.profile.token) .pipe(
      catchError((err: HttpErrorResponse, caught: Observable<any>) => {
        if (err.status == 401 || err.status == 500) {

          // Backend rejected the token, clear it
          this.userService.clearAdminToken();
          this.openDialog();

        } else {
          this._snackBar.open(err.message, undefined, {duration: 5 * 1000})
          console.error(err)
        }
        return throwError(() => new Error(err.message));
      })
    ).subscribe((d: ConfigBody) => {

      this.configJson = d;
      this.config.setValue(d.config.admin)
      console.log(d);
    })
  };

  onLogoutClick() {
    this.userService.clearAdminToken();
    this.router.navigate(['/']);
  }

  protected readonly JSON = JSON;

  onSubmit = () => {
    const values = this.config.value
    console.log('submit', values);
    if (!this.userService.profile.token) {
      throw new Error('no token')
    }
    const newConfig = {
      admin: values,
    }
    this.karaokeService.postConfig(this.userService.profile.token, newConfig as ConfigData)
      .pipe(catchError((err: HttpErrorResponse, caught) =>{
          this._snackBar.open(err.message, 'Ok',)
        return throwError(() => err);
        }
      ))
      .subscribe((response) => {
        this._snackBar.open('Config updated', undefined, {duration: 5 * 1000})
        this.configJson = response;
        this.config.setValue(response.config.admin);
        if (response.token){
          this.userService.profile.token = response.token;
          this.userService.save();
        }

      })
  };
}
