import { TestBed } from '@angular/core/testing';

import { KaraokeService } from './karaoke.service';
import {HttpClientModule} from "@angular/common/http";

describe('KaraokeService', () => {
  beforeEach(() => TestBed.configureTestingModule(
    {imports: [
      HttpClientModule,
      ]}
  ));

  it('should be created', () => {
    const service: KaraokeService = TestBed.get(KaraokeService);
    expect(service).toBeTruthy();
  });
});
