import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {QueueEntryComponent} from './queue-entry.component';
import {BrowserModule} from "@angular/platform-browser";
import {AppRoutingModule} from "../app-routing.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatMenuModule} from "@angular/material/menu";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import {FormsModule} from "@angular/forms";
import {MatTableModule} from "@angular/material/table";
import {MatListModule} from "@angular/material/list";
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetModule, MatBottomSheetRef,} from "@angular/material/bottom-sheet";
import {MatDialogModule} from "@angular/material/dialog";
import {MatTabsModule} from "@angular/material/tabs";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {QueueEntry} from '../status';

describe('QueueEntryComponent', () => {
  const mockBottomSheetRef = {
    dismiss: jasmine.createSpy('dismiss')
  };

  let component: QueueEntryComponent;
  let fixture: ComponentFixture<QueueEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatToolbarModule,
        MatMenuModule,
        MatIconModule,
        MatButtonModule,
        MatInputModule,
        FormsModule,
        MatTableModule,
        MatListModule,
        MatBottomSheetModule,
        MatDialogModule,
        MatTabsModule,
        MatProgressSpinnerModule,

      ],
      declarations: [QueueEntryComponent],
      providers: [
        {
          provide: MatBottomSheetRef,
          useValue: mockBottomSheetRef
        },
        {
          provide: MAT_BOTTOM_SHEET_DATA,
          useValue: QueueEntryComponent,
        },

      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueueEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
