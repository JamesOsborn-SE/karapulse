import {Component, OnInit} from '@angular/core';
import {KaraokeService} from '../karaoke.service';
import {Song} from '../song';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  results: Song[] = [];
  searchTerms$ = new Subject<string>();
  isLoading = false;

  constructor(private karaokeService: KaraokeService) {
    this.karaokeService.search(this.searchTerms$)
      .subscribe(results => {
        console.dir({results});
        if ('isLoading' in results) {
          this.isLoading = results.isLoading;
        } else if (typeof results === 'object') {
          this.results = results;
          this.isLoading = false;
        }
      });
  }

  ngOnInit() {

  }

  log(txt: string) {
    console.log(txt);
  }
}
