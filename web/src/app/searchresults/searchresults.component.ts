import {Component, Input, OnInit} from '@angular/core';
import {Song} from '../song';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {SongDetailsComponent} from '../song-details/song-details.component';


@Component({
  selector: 'app-searchresults',
  templateUrl: './searchresults.component.html',
  styleUrls: ['./searchresults.component.css'],
})
export class SearchResultsComponent implements OnInit {
  @Input() results: Song[] = [];
  displayedColumns: string[] = ['card'];
  constructor(private bottomSheet: MatBottomSheet) {}

  ngOnInit() {}

  openBottomSheet(row: any): void {
    console.log(row);
    this.bottomSheet.open(SongDetailsComponent, { data: row });
  }

  etaAsStr(eta: number): string {
    // Convert second as 12:13
    const hours = Math.floor(eta / 3600);
    const remaining = eta % 3600;
    const minutes = Math.floor(remaining / 60);
    const second = eta % 60;
    const secondStr = `${second}`.padStart(2, '0');
    if (hours == 0) {
      return `${minutes}:${secondStr}`;
    } else {
      const minutesStr = `${minutes}`.padStart(2, '0');
      return `${hours}:${minutesStr}:${secondStr}`;
    }
  }
}
