import {Injectable} from '@angular/core';
import {Profile} from './profile';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  profile: Profile;

  constructor() {
    // if the profile, or the use of the localstorage grows, we should use a dedicated typescript or ng lib to handle serialization;
    this.profile = new Profile();

    const savedName = localStorage.getItem('profile.name');
    const token = localStorage.getItem('profile.token');
    this.profile.name = savedName ? savedName : 'default';
    this.profile.token = token ? token  : undefined;
  }

  save(): void {
    localStorage.setItem('profile.name', this.profile.name ? this.profile.name : 'Céline Dion');
    localStorage.setItem('profile.token', this.profile.token ?? '');
  }

  clearAdminToken() {
    console.log('clearing admin token');
    this.profile.token = undefined;
    this.save()
    // should redirect
  }
}
